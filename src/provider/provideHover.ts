// Declare variables
import * as vscode from 'vscode';
import {CancellationToken, Hover, Position, ProviderResult, TextDocument} from "vscode";
const docs = require('../doc/docs');

export class ProvideHover implements vscode.HoverProvider {
    provideHover( document: TextDocument, position: Position, token: CancellationToken ): ProviderResult<Hover> {
        return new Promise(resolve => {
            const commentIndex = document.lineAt(position.line).text.indexOf('//');
            if ( commentIndex >= 0 && position.character > commentIndex ) {
                resolve(undefined);
            }
            const range = document.getWordRangeAtPosition(position);
            if ( range === undefined ) {
                resolve(undefined);
            }
            const word = document.lineAt( position.line ).text.slice( 0, range!.end.character ).match(/[\w.]+$/g)![0];
            for ( let i = 0; i < docs.squirrel.length; i++ ) {
                if ( word === docs.squirrel[i].name || word === docs.squirrel[i].longname ) {
                    resolve(docs.squirrel[i].VShover);
                }
            }
            resolve(undefined);
        });
    }
}
