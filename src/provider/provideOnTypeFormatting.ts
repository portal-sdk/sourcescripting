// Declare variables
import * as vscode from 'vscode';
import {
    CancellationToken,
    FormattingOptions,
    OnTypeFormattingEditProvider,
    Position,
    ProviderResult,
    TextDocument, TextEdit
} from "vscode";
import { js_beautify } from 'js-beautify';

export class ProvideOnTypeFormattingEdits implements OnTypeFormattingEditProvider {
    provideOnTypeFormattingEdits(document: TextDocument, position: Position, ch: string, options: FormattingOptions, token: CancellationToken): ProviderResult<TextEdit[]> {
        return new Promise(resolve => {
            const fullText = document.lineAt( position.line ).text;
            resolve([
                vscode.TextEdit.replace(
                    new vscode.Range( position.line, 0, position.line, fullText.length ),
                    js_beautify(fullText)
                )
            ]);
        });
    }
}
