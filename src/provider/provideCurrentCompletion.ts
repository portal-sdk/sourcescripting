// Declare variables
import {
    CancellationToken,
    CompletionContext, CompletionItem, CompletionItemKind,
    CompletionItemProvider,
    Position,
    ProviderResult,
    TextDocument
} from "vscode";

export class ProvideCurrentCompletion implements CompletionItemProvider {
    provideCompletionItems(document: TextDocument, position: Position, token: CancellationToken, context: CompletionContext): ProviderResult<CompletionItem[]> {
        return new Promise(resolve => {
            let type = [ CompletionItemKind.Function, CompletionItemKind.Class ];
            let match;
            let completions: CompletionItem[] = [];
            for ( let reg of [ /(?<=function\s)\w*/g, /(?<=class\s)\w*/g ] ) {
                while ( match = reg.exec( document.getText() ) ) {
                    // TODO: Shorten this cuz ugly
                    completions.push( new CompletionItem( match[0], type[ reg.source.includes('class') as unknown as number ] ) );
                }
            }
            resolve(completions);
        });
    }
}
