import {
    CancellationToken,
    CompletionContext, CompletionItem,
    CompletionItemProvider,
    Position,
    ProviderResult,
    TextDocument
} from "vscode";
import { builtin } from '../doc';
import Variable from '../item/Item';

let globalCompletion: CompletionItem[] = [];
for ( let thing of builtin ) {
    if ( thing.scope === "global" || thing.scope === "static" ) {
        globalCompletion.push( thing.VScompletion );
    }
}

export class ProvideCompletionItems implements CompletionItemProvider {
    provideCompletionItems(document: TextDocument, position: Position, token: CancellationToken, context: CompletionContext): ProviderResult<CompletionItem[]> {
        return globalCompletion;
    }
}

export class ProvideCompletionItemsDOT implements CompletionItemProvider{
    provideCompletionItems(document: TextDocument, position: Position, token: CancellationToken, context: CompletionContext): ProviderResult<CompletionItem[]> {
        return new Promise(resolve => {
            let line = document.lineAt(position.line);
            let dotIdx = line.text.lastIndexOf('.', position.character);
            if (dotIdx === -1) {
                resolve([]);
            }
            const commentIndex = line.text.indexOf('//');
            if (commentIndex >= 0 && position.character > commentIndex) {
                resolve([]);
            }
            const range = document.getWordRangeAtPosition(position.with(position.line, position.character - 1));
            const word = document.getText(range);
            let variable = new Variable(document);
            let globalCompletion2 = [];
            for ( let x = 0; x < variable.list.length; x++ ) {
                if ( word === variable.list[x].name ) {
                    for ( let thing of builtin ) {
                        if ( thing.memberof === variable.list[x].value ) {
                            globalCompletion2.push( thing.VScompletion );
                        }
                    }
                    resolve(globalCompletion2);
                }
            }
            resolve([]);
        });
    }
}
