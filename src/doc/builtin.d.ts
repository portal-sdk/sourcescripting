// noinspection JSUnusedGlobalSymbols

type int = number
type float = number
type bytes = Int8Array

/**
 * compiles a squirrel script or loads a precompiled one and executes it. When squirrel is compiled in unicode mode the function can handle different character ecodings, UTF8 with and without prefix and UCS-2 prefixed(both big endian an little endian). If the source stream is not prefixed UTF8 ecoding is used as default.
 * @param {string} path
 * @param {boolean} [raiseerror] if is true, the compiler error handler is invoked in case of a syntax error. If is omitted or set to false, the compiler error handler is not ivoked.
 * @return {int} returns the value returned by the script or null if no value is returned
 */
declare function dofile(path: string, raiseerror?: boolean): int;

/**
 * compiles a squirrel script or loads a precompiled one an returns it as as function. When squirrel is compiled in unicode mode the function can handle different character ecodings, UTF8 with and without prefix and UCS-2 prefixed(both big endian an little endian). If the source stream is not prefixed UTF8 ecoding is used as default.
 * @param {string} path
 * @param {boolean} [raiseerror] if the optional parameter ‘raiseerror’ is true, the compiler error handler is invoked in case of a syntax error. If raiseerror is omitted or set to false, the compiler error handler is not ivoked.
 */
declare function loadfile(path: string, raiseerror?: boolean): void;

/**
 * serializes a closure to a bytecode file (destpath). The serialized file can be loaded using loadfile() and dofile().
 * @param {string} destpath
 * @param {any} closure
 */
declare function writeclosuretofile(destpath: string, closure: any): void;

/**
 * casts a float to a int
 * @param {float} f
 * @return {int}
 */
declare function castf2i(f: float): int;

/**
 * casts a int to a float
 * @param {int} n
 * @return {float}
 */
declare function casti2f(n: int): float;

/**
 * swap the byte order of a number (like it would be a 16bits integer)
 * @param {int} n
 * @return {int}
 */
declare function swap2(n: int): int;

/**
 * swap the byte order of an integer
 * @param {int} n
 * @return {int}
 */
declare function swap4(n: int): int;

/**
 * swaps the byteorder of a float
 * @param {float} f
 * @return {float}
 */
declare function swapfloat(f: float): float;

/**
 * returns the absolute value of x as an integer
 * @param {int} x
 * @return {float}
 */
declare function abs(x: int): float;

/**
 * returns the arccosine of x
 * @param {int} x
 * @return {float}
 */
declare function acos(x: int): float;

/**
 * returns the arcsine of x
 * @param {int} x
 * @return {float}
 */
declare function asin(x: int): float;

/**
 * returns the arctangent of x
 * @param {int} x
 * @return {float}
 */
declare function atan(x: int): float;

/**
 * returns the arctangent of x/y
 * @param {int} x
 * @param {int} y
 * @return {float}
 */
declare function atan2(x: int, y: int): float;

/**
 * returns a float value representing the smallest integer that is greater than or equal to x
 * @param {int} x
 * @return {float}
 */
declare function ceil(x: int): float;

/**
 * returns the cosine of x
 * @param {int} x
 * @return {float}
 */
declare function cos(x: int): float;

/**
 * returns the exponential value of the float parameter x
 * @param {int} x
 * @return {float}
 */
declare function exp(x: int): float;

/**
 * returns the absolute value of x as a float
 * @param {int} x
 * @return {float}
 */
declare function fabs(x: int): float;

/**
 * returns a float value representing the largest integer that is less than or equal to x
 * @param {int} x
 * @return {float}
 */
declare function floor(x: int): float;

/**
 * returns the natural logarithm of x
 * @param {int} x
 * @return {float}
 */
declare function log(x: int): float;

/**
 * returns the logarithm base-10 of x
 * @param {int} x
 * @return {float}
 */
declare function log10(x: int): float;

/**
 * returns x raised to the power of y
 * @param {int} x
 * @return {float}
 */
declare function pow(x: int): float;

/**
 * returns a pseudorandom integer in the range 0 to RAND_MAX
 * @return {int}
 */
declare function rand(x: any): int;

/**
 * returns the sine of x
 * @param {int} x
 * @return {float}
 */
declare function sin(x: int): float;

/**
 * returns the square root of x
 * @param {int} x
 * @return {float}
 */
declare function sqrt(x: int): float;

/**
 * sets the starting point for generating a series of pseudorandom integers
 * @param {int} seed
 * @return {int}
 */
declare function srand(seed: int): int;

/**
 * returns the tangent of x
 * @param {int} x
 * @return {float}
 */
declare function tan(x: int): float;

/**
 * returns a float representing the number of seconds elapsed since the start of the process
 * @return {float}
 */
declare function clock(): float;

/**
 * returns a table containing a date/time splitted in the slots
 * @param {int} [time] if omitted the current time is used.
 * @param {string} [format] if format can be ‘l’ local time or ‘u’ UTC time, if omitted is defaulted as ‘l’(local time).
 * @return {Array}
 */
declare function date(time?: int, format?: string): any[];

/**
 * Returns a string containing the value of the environment variable varname
 * @param {int} varaname
 * @return {string}
 */
declare function getenv(varaname: int): string;

/**
 * deletes the file specified by path
 * @param {string} path
 */
declare function remove(path: string): void;

/**
 * renames the file or directory specified by oldname to the name given by newname
 * @param {string} oldname
 * @param {string} newname
 */
declare function rename(oldname: string, newname: string): void;

/**
 * excutes the string cmd through the os command interpreter.
 * @param {string} cmd
 */
declare function system(cmd: string): void;

/**
 * returns the number of seconds elapsed since midnight 00:00:00, January 1, 1970. the result of this function can be formatted through the function date()
 * @return {string}
 */
declare function time(): string;

/**
 * returns true if the end of the string str matches a the string cmp otherwise returns false
 * @param {string} str
 * @param {string} cmp
 * @return {boolean}
 */
declare function endswith(str: string, cmp: string): boolean;

/**
 * Returns a string with backslashes before characters that need to be escaped(”,a,b,t,n,v,f,r,\,”,’,0,xnn).
 * @param {string} str
 * @return {string}
 */
declare function ecape(str: string): string;

/**
 * Returns a string formatted according formatstr and the optional parameters following it. The format string follows the same rules as the printf family of standard C functions( the “*” is not supported).
 * @param {string} formatstr
 * @param {...string} optional
 * @return {string}
 * @example
 * sq> print(format("%s %d 0x%02X\n","this is a test :",123,10));
 * // > this is a test : 123 0x0A
 */
declare function format(formatstr: string, optional: string[]): string;

/**
 * Strips white-space-only characters that might appear at the beginning of the given string and returns the new stripped string.
 * @param {string} str
 * @return {string}
 */
declare function lstrip(str: string): string;

/**
 * Strips white-space-only characters that might appear at the end of the given string and returns the new stripped string.
 * @param {string} str
 * @return {string}
 */
declare function rstrip(str: string): string;

/**
 * returns an array of strings split at each point where a separator character occurs in str. The separator is not returned as part of any array element.
 * @param {string} str
 * @param {string} separtators The parameter separators is a string that specifies the characters as to be used for the splitting.
 * @return {string}
 * @example
 * local a = split("1.2-3;4/5",".-/;"); // the result will be  [1,2,3,4,5]
 */
declare function split(str: string, separtators: string): string;

/**
 * returns true if the beginning of the string str matches a the string cmp otherwise returns false
 * @param {string} str
 * @param {string} cmp
 * @return {boolean}
 */
declare function startswith(str: string, cmp: string): boolean;

/**
 * Strips white-space-only characters that might appear at the beginning or end of the given string and returns the new stripped string.
 * @param {string} str
 * @return {string}
 */
declare function strip(str: string): string;

declare class file {
    /**
    * creates a file with read/write access in the current directory. It’s contructor imitates the behaviour of the C runtime function fopen for eg.
    * @param  {string} path
    * @param  {string} patten
    * @return {any}
    * @example
    * local myfile = file("test.xxx","wb+");
     *
    */
    constructor(path: string, patten: string);

    /**
    * closes the file.
    */
    close(): void;

    /**
    * returns a non null value if the read/write pointer is at the end of the stream.
    * @return {int}
    */
    eos(): int;

    /**
    * flushes the stream.return a value != null if succeded, otherwise returns null
    * @return {int}
    */
    flush(): int;

    /**
    * returns the lenght of the stream
    * @return {int}
    */
    len(): int;

    /**
    * read n bytes from the stream and retuns them as blob
    * @param  {int} size
    * @return {bytes}
    */
    readblob(size: int): bytes;

    /**
    * reads a number from the stream according to the type pameter.
    * @param  {int} type type of the number to read
    */
    readn(type: int): void;

    /**
   * resizes the blob to the specified size
   * @param  {int} size the new size of the blobl in bytes
   */
    resize(size: int): void;

    /**
    * Moves the read/write pointer to a specified location.
    * @param  {int} offset the new size of the blobl in bytes
    * @param  {int} [origin] the new size of the blobl in bytes
    */
    seek(offset: int, origin?: int): void;

    /**
    * returns the read/write pointer absolute position
    * @return {any}
    */
    tell(): any;

    /**
     * Moves the read/write pointer to a specified location.
     * @param  {blob} src the source blob containing the data to be written
     */
    writeblob(src: blob): void;

    /**
     * writes a number in the stream formatted according to the type pameter
     * @param  {int} n the value to be written
     * @param  {int} type type of the number to write
 
     */
    writen(n: int, type: int): void;

}
declare class blob {
    /**
    * returns a new instance of a blob class of the specified size in bytes
    * @param  {string} size
    * @return {any}
    */
    constructor(size: string);

    /**
    * returns a non null value if the read/write pointer is at the end of the stream.
    * @return {int}
    */
    eos(): int;

    /**
    * flushes the stream.return a value != null if succeded, otherwise returns null
    * @return {int}
    */
    flush(): int;

    /**
    * returns the lenght of the stream
    * @return {int}
    */
    len(): int;

    /**
     * read n bytes from the stream and retuns them as blob
     * @param  {int} size
     * @return {bytes}
     */
    readblob(size: int): bytes;

    /**
    * reads a number from the stream according to the type pameter.
    * @param  {int} type type of the number to read
    */
    readn(type: int): void;

    /**
   * resizes the blob to the specified size
   * @param  {int} size the new size of the blobl in bytes
   */
    resize(size: int): void;

    /**
    * Moves the read/write pointer to a specified location.
    * @param  {int} offset the new size of the blobl in bytes
    * @param  {int} [origin] the new size of the blobl in bytes
    */
    seek(offset: int, origin?: int): void;

    /**
    * swaps the byte order of the blob content as it would be an array of 16bits integers
    * @return {int}
    */
    swap2(): int;

    /**
    * swaps the byte order of the blob content as it would be an array of 32bits integers
    * @return {int}
    */
    swap4(): int;

    /**
    * returns the read/write pointer absolute position
    * @return {any}
    */
    tell(): any;

    /**
     * Moves the read/write pointer to a specified location.
     * @param  {blob} src the source blob containing the data to be written
     */
    writeblob(src: blob): void;

    /**
     * writes a number in the stream formatted according to the type pameter
     * @param  {int} n the value to be written
     * @param  {int} type type of the number to write
 
     */
    writen(n: int, type: int): void;

}
declare class regexp {
    /**
    * The regexp object rapresent a precompiled regular experssion pattern. The object is created trough regexp(pattern).
    * @param  {string} pattern
    * @return {any}
    */
    constructor(pattern: string);

    /**
    * Returns an array of tables containing two indexs(“begin” and “end”)of the first match of the regular expression in the string str. An array entry is created for each captured sub expressions. If no match occurs returns null. The first element of the returned array(index 0) always contains the complete match.
    * @param  {string} str
    * @param  {string} [start] The search starts from the index start of the string, if start is omitted the search starts from the beginning of the string.
    * @return {Array}
    * @example
    * local ex = regexp(@"(\d+) ([a-zA-Z]+)(\p)");
    * local string = "stuff 123 Test;";
    * local res = ex.capture(string);
    * foreach(i,val in res)
    * {
    *     print(format("match number[%02d] %s\n",
    *             i,string.slice(val.begin,val.end))); //prints "Test"
    * }
    */
    capture(str: string, start?: string): any[];

    /**
    * Returns a true if the regular expression matches the string str, otherwise returns false.
    * @param  {string} str
    * @return {boolean}
    */
    match(str: string): boolean;

    /**
    * Returns a table containing two indexs(“begin” and “end”) of the first match of the regular expression in the string str, otherwise if no match occurs returns null.
    * @param  {string} str
    * @param  {string} [start] The search starts from the index start of the string, if start is omitted the search starts from the beginning of the string.
    * @return {Array}
    * @example
    * local string = "123 Test;";
    * local res = ex.search(string);
    * print(string.slice(res.begin,res.end)); //prints "Test"
    */
    search(str: string, start?: string): any[];

}
