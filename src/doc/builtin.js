/**
 * compiles a squirrel script or loads a precompiled one and executes it. When squirrel is compiled in unicode mode the function can handle different character ecodings, UTF8 with and without prefix and UCS-2 prefixed(both big endian an little endian). If the source stream is not prefixed UTF8 ecoding is used as default.
 * @param {string} path
 * @param {boolean} [raiseerror] if is true, the compiler error handler is invoked in case of a syntax error. If is omitted or set to false, the compiler error handler is not ivoked.
 * @return {int} returns the value returned by the script or null if no value is returned
 */
function dofile(path, raiseerror) { }

/**
 * compiles a squirrel script or loads a precompiled one an returns it as as function. When squirrel is compiled in unicode mode the function can handle different character ecodings, UTF8 with and without prefix and UCS-2 prefixed(both big endian an little endian). If the source stream is not prefixed UTF8 ecoding is used as default.
 * @param {string} path
 * @param {boolean} [raiseerror] if the optional parameter ‘raiseerror’ is true, the compiler error handler is invoked in case of a syntax error. If raiseerror is omitted or set to false, the compiler error handler is not ivoked.
 */
function loadfile(path, raiseerror) { }

/**
 * serializes a closure to a bytecode file (destpath). The serialized file can be loaded using loadfile() and dofile().
 * @param {string} destpath
 * @param {any} closure
 */
function writeclosuretofile(destpath, closure) { }


class file {
    /**
    * creates a file with read/write access in the current directory. It’s contructor imitates the behaviour of the C runtime function fopen for eg.
    * @param  {string} path
    * @param  {string} patten
    * @return {any}
    * @example
    * local myfile = file("test.xxx","wb+");
    */
    constructor(path, patten) { }

    /**
    * closes the file.
    */
    close() { }

    /**
    * returns a non null value if the read/write pointer is at the end of the stream.
    * @return {int}
    */
    eos() { }

    /**
    * flushes the stream.return a value != null if succeded, otherwise returns null
    * @return {int}
    */
    flush() { }

    /**
    * returns the lenght of the stream
    * @return {int}
    */
    len() { }

    /**
    * read n bytes from the stream and retuns them as blob
    * @param  {int} size
    * @return {bytes}
    */
    readblob(size) { }

    /**
    * reads a number from the stream according to the type pameter.
    * @param  {int} type type of the number to read
    */
    readn(type) { }

    /**
   * resizes the blob to the specified size
   * @param  {int} size the new size of the blobl in bytes
   */
    resize(size) { }

    /**
    * Moves the read/write pointer to a specified location.
    * @param  {int} offset the new size of the blobl in bytes
    * @param  {int} [origin] the new size of the blobl in bytes
    */
    seek(offset, origin) { }

    /**
    * returns the read/write pointer absolute position
    * @return {any}
    */
    tell() { }

    /**
     * Moves the read/write pointer to a specified location.
     * @param  {blob} src the source blob containing the data to be written
     */
    writeblob(src) { }

    /**
     * writes a number in the stream formatted according to the type pameter
     * @param  {int} n the value to be written
     * @param  {int} type type of the number to write
 
     */
    writen(n, type) { }
}

/**
 * casts a float to a int
 * @param {float} f
 * @return {int}
 */
function castf2i(f) { }

/**
 * casts a int to a float
 * @param {int} n
 * @return {float}
 */
function casti2f(n) { }

/**
 * swap the byte order of a number (like it would be a 16bits integer)
 * @param {int} n
 * @return {int}
 */
function swap2(n) { }

/**
 * swap the byte order of an integer
 * @param {int} n
 * @return {int}
 */
function swap4(n) { }

/**
 * swaps the byteorder of a float
 * @param {float} f
 * @return {float}
 */
function swapfloat(f) { }


class blob {
    /**
    * returns a new instance of a blob class of the specified size in bytes
    * @param  {string} size
    * @return {any}
    */
    constructor(size) { }

    /**
    * returns a non null value if the read/write pointer is at the end of the stream.
    * @return {int}
    */
    eos() { }

    /**
    * flushes the stream.return a value != null if succeded, otherwise returns null
    * @return {int}
    */
    flush() { }

    /**
    * returns the lenght of the stream
    * @return {int}
    */
    len() { }

    /**
     * read n bytes from the stream and retuns them as blob
     * @param  {int} size
     * @return {bytes}
     */
    readblob(size) { }

    /**
    * reads a number from the stream according to the type pameter.
    * @param  {int} type type of the number to read
    */
    readn(type) { }

    /**
   * resizes the blob to the specified size
   * @param  {int} size the new size of the blobl in bytes
   */
    resize(size) { }

    /**
    * Moves the read/write pointer to a specified location.
    * @param  {int} offset the new size of the blobl in bytes
    * @param  {int} [origin] the new size of the blobl in bytes
    */
    seek(offset, origin) { }

    /**
    * swaps the byte order of the blob content as it would be an array of 16bits integers
    * @return {int}
    */
    swap2() { }

    /**
    * swaps the byte order of the blob content as it would be an array of 32bits integers
    * @return {int}
    */
    swap4() { }

    /**
    * returns the read/write pointer absolute position
    * @return {any}
    */
    tell() { }

    /**
     * Moves the read/write pointer to a specified location.
     * @param  {blob} src the source blob containing the data to be written
     */
    writeblob(src) { }

    /**
     * writes a number in the stream formatted according to the type pameter
     * @param  {int} n the value to be written
     * @param  {int} type type of the number to write
 
     */
    writen(n, type) { }
}

/**
 * returns the absolute value of x as an integer
 * @param {int} x
 * @return {float}
 */
function abs(x) { }

/**
 * returns the arccosine of x
 * @param {int} x
 * @return {float}
 */
function acos(x) { }

/**
 * returns the arcsine of x
 * @param {int} x
 * @return {float}
 */
function asin(x) { }

/**
 * returns the arctangent of x
 * @param {int} x
 * @return {float}
 */
function atan(x) { }

/**
 * returns the arctangent of x/y
 * @param {int} x
 * @param {int} y
 * @return {float}
 */
function atan2(x, y) { }

/**
 * returns a float value representing the smallest integer that is greater than or equal to x
 * @param {int} x
 * @return {float}
 */
function ceil(x) { }

/**
 * returns the cosine of x
 * @param {int} x
 * @return {float}
 */
function cos(x) { }

/**
 * returns the exponential value of the float parameter x
 * @param {int} x
 * @return {float}
 */
function exp(x) { }

/**
 * returns the absolute value of x as a float
 * @param {int} x
 * @return {float}
 */
function fabs(x) { }

/**
 * returns a float value representing the largest integer that is less than or equal to x
 * @param {int} x
 * @return {float}
 */
function floor(x) { }

/**
 * returns the natural logarithm of x
 * @param {int} x
 * @return {float}
 */
function log(x) { }

/**
 * returns the logarithm base-10 of x
 * @param {int} x
 * @return {float}
 */
function log10(x) { }

/**
 * returns x raised to the power of y
 * @param {int} x
 * @return {float}
 */
function pow(x) { }

/**
 * returns a pseudorandom integer in the range 0 to RAND_MAX
 * @return {int}
 */
function rand(x) { }

/**
 * returns the sine of x
 * @param {int} x
 * @return {float}
 */
function sin(x) { }

/**
 * returns the square root of x
 * @param {int} x
 * @return {float}
 */
function sqrt(x) { }

/**
 * sets the starting point for generating a series of pseudorandom integers
 * @param {int} seed
 * @return {int}
 */
function srand(seed) { }

/**
 * returns the tangent of x
 * @param {int} x
 * @return {float}
 */
function tan(x) { }


/*
 * the maximum value that can be returned by the rand() function
 * @constant
 * @type {int}
const RAND_MAX
 */

/*
 * The numeric constant pi (3.141592) is the ratio of the circumference of a circle to its diameter
 * @constant
 * @type {int}
const PI
*/

/**
 * returns a float representing the number of seconds elapsed since the start of the process
 * @return {float}
 */
function clock() { }

/**
 * returns a table containing a date/time splitted in the slots
 * @param {int} [time] if omitted the current time is used.
 * @param {string} [format] if format can be ‘l’ local time or ‘u’ UTC time, if omitted is defaulted as ‘l’(local time).
 * @return {Array}
 */
function date(time, format) { }

/**
 * Returns a string containing the value of the environment variable varname
 * @param {int} varaname
 * @return {string}
 */
function getenv(varaname) { }

/**
 * deletes the file specified by path
 * @param {string} path
 */
function remove(path) { }

/**
 * renames the file or directory specified by oldname to the name given by newname
 * @param {string} oldname
 * @param {string} newname
 */
function rename(oldname, newname) { }

/**
 * excutes the string cmd through the os command interpreter.
 * @param {string} cmd
 */
function system(cmd) { }

/**
 * returns the number of seconds elapsed since midnight 00:00:00, January 1, 1970. the result of this function can be formatted through the function date()
 * @return {string}
 */
function time() { }

/**
 * returns true if the end of the string str matches a the string cmp otherwise returns false
 * @param {string} str
 * @param {string} cmp
 * @return {boolean}
 */
function endswith(str, cmp) { }

/**
 * Returns a string with backslashes before characters that need to be escaped(”,a,b,t,n,v,f,r,\,”,’,0,xnn).
 * @param {string} str
 * @return {string}
 */
function ecape(str) { }


/**
 * Returns a string formatted according formatstr and the optional parameters following it. The format string follows the same rules as the printf family of standard C functions( the “*” is not supported).
 * @param {string} formatstr
 * @param {...string} optional
 * @return {string}
 * @example
 * sq> print(format("%s %d 0x%02X\n","this is a test :",123,10)); // > this is a test : 123 0x0A
 */
function format(formatstr, optional) { }

/**
 * Strips white-space-only characters that might appear at the beginning of the given string and returns the new stripped string.
 * @param {string} str
 * @return {string}
 */
function lstrip(str) { }

/**
 * Strips white-space-only characters that might appear at the end of the given string and returns the new stripped string.
 * @param {string} str
 * @return {string}
 */
function rstrip(str) { }

/**
 * returns an array of strings split at each point where a separator character occurs in str. The separator is not returned as part of any array element.
 * @param {string} str
 * @param {string} separtators The parameter separators is a string that specifies the characters as to be used for the splitting.
 * @return {string}
 * @example 
 * local a = split("1.2-3;4/5",".-/;"); // the result will be  [1,2,3,4,5]
 */
function split(str, separtators) { }

/**
 * returns true if the beginning of the string str matches a the string cmp otherwise returns false
 * @param {string} str
 * @param {string} cmp
 * @return {boolean}
 */
function startswith(str, cmp) { }

/**
 * Strips white-space-only characters that might appear at the beginning or end of the given string and returns the new stripped string.
 * @param {string} str
 * @return {string}
 */
function strip(str) { }


class regexp {
    /**
    * The regexp object rapresent a precompiled regular experssion pattern. The object is created trough regexp(patern).
    * @param  {string} pattern
    * @return {any}
    */
    constructor(pattern) { }

    /**
    * Returns an array of tables containing two indexs(“begin” and “end”)of the first match of the regular expression in the string str. An array entry is created for each captured sub expressions. If no match occurs returns null. The first element of the returned array(index 0) always contains the complete match.
    * @param  {string} str
    * @param  {string} [start] The search starts from the index start of the string, if start is omitted the search starts from the beginning of the string.
    * @return {Array}
    * @example
    * local ex = regexp(@"(\d+) ([a-zA-Z]+)(\p)");
    * local string = "stuff 123 Test;";
    * local res = ex.capture(string);
    * foreach(i,val in res)
    * {
    *     print(format("match number[%02d] %s\n",
    *             i,string.slice(val.begin,val.end))); //prints "Test"
    * }
    */
   capture(str, start) { }
 
    /**
    * Returns a true if the regular expression matches the string str, otherwise returns false.
    * @param  {string} str
    * @return {boolean}
    */
   match(str) { }

    /**
    * Returns a table containing two indexs(“begin” and “end”) of the first match of the regular expression in the string str, otherwise if no match occurs returns null.
    * @param  {string} str
    * @param  {string} [start] The search starts from the index start of the string, if start is omitted the search starts from the beginning of the string.
    * @return {Array}
    * @example
    * local string = "123 Test;";
    * local res = ex.search(string);
    * print(string.slice(res.begin,res.end)); //prints "Test"
    */
    search(str, start) { }
}