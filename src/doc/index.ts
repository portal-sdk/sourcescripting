import JSDOCExtend from './JSDOCExtend';
import { Application } from 'typedoc';
import { join } from 'path';

let app = new Application();
app.bootstrap( { entryPoints: [ __dirname + 'builtin.d.ts' ] } );
let ref = app.convert()!;

// TODO: Reimplement this in a less shitty way

export const builtin = new JSDOCExtend( join(__dirname, '/builtin.d.ts' ) ).docs;
export const chaos = new JSDOCExtend( join(__dirname, '/chaos.d.ts' ) ).docs;
export const portal2 = new JSDOCExtend( join(__dirname, '/portal2.d.ts' ) ).docs;
