import * as vscode from 'vscode';
import * as fs from 'fs';
import * as beautify from 'js-beautify';
import {Hover} from "vscode";

const jsdoc = require('jsdoc-api');

interface DocObject {
    name: string;
    params: String[];
    description: string;
    head: any;
    scope: any;
    kind: string;
    documentation: any;
    VScompletion: any;
    VShover: any;
    VSsignatureInformation: any;
    longname: any;
    meta: any;
    memberof: any;
}

export default class JSDOCExtend {
    docs: DocObject[];

    constructor( path: string ) {
        let text = fs.readFileSync(path).toString();
        this.docs = jsdoc.explainSync({ source: text });
        this.docs = this.docs.splice(0, this.docs.length - 1);
        for (let i = 0; i < this.docs.length; i++) {
            this.docs[i] = fixMember(this.docs[i], text);
            this.docs[i].head = getHead(this.docs[i]);
            this.docs[i].documentation = getDocumentation(this.docs[i]);
            this.docs[i].VScompletion = getCompletion(this.docs[i]);
            this.docs[i].VShover = getHover(this.docs[i]);
            this.docs[i].VSsignatureInformation = getSignatureInformation(this.docs[i]);
        }
    }
}

function fixMember( obj: DocObject, text: string ): DocObject {
    if (obj.kind === 'member' && obj.scope === 'instance') {
        if (text.substring(obj.meta.range[0], obj.meta.range[1]).includes('static')) {
            obj.longname = obj.longname.split('#').join('.');
            obj.scope = 'static';
        }
    }
    const regex = /(\w+)\s*=\s*(\w+\(.*\)){1}/gm;
    let myArray;
    if (obj.kind === 'member' && obj.meta.code.value === '' && obj.scope === 'global') {
        while (myArray = regex.exec(text.substring(obj.meta.range[0], obj.meta.range[1]))) {
            obj.meta.code.value = myArray[2];

        }
    }
    else if (obj.kind === 'member' && obj.meta.code.value === undefined && obj.scope === 'instance') {
        while (myArray = regex.exec(text.substring(obj.meta.range[0], obj.meta.range[1]))) {
            obj.meta.code.value = myArray[2];
        }
    }
    return obj;
}

function getSignatureInformation( obj: DocObject ) {
    if ( obj.kind === 'function' ) {
        let signatureInformation = new vscode.SignatureInformation(obj.head, new vscode.MarkdownString(obj.description));
        for ( let j = 0; j < obj.params.length; j++ ) {
            signatureInformation.parameters.push(new vscode.ParameterInformation(j, obj.params[j].description));
        }
        return signatureInformation;
    }
    return undefined;
}

function getHover( obj: DocObject ): Hover {
    const code = new vscode.MarkdownString();
    code.appendMarkdown("``` squirrel\n" + obj.head + "\n```");
    if (obj.documentation != undefined) {
        code.appendMarkdown("\n" + obj.documentation);
    }
    return new Hover(code.value);
}

function getHead( obj: DocObject ) {
    let code = new vscode.MarkdownString();
    code.appendMarkdown(obj.kind + " " + obj.name);
    if (obj.kind === "function") {
        if (obj.scope === "static") {
            code = new vscode.MarkdownString();
            code.appendMarkdown(obj.kind + " " + obj.longname);
        }
        code.appendMarkdown("(");
        if (obj.params != undefined) {
            for (var i = 0; i < obj.params.length; i++) {
                code.appendMarkdown(obj.params[i].name);
                if (obj.params[i].optional)
                    code.appendMarkdown("?");
                code.appendMarkdown(": " + obj.params[i].type.names[0]);
                if (i < obj.params.length - 1)
                    code.appendMarkdown(", ");
            }
        }
        if (obj.returns != undefined) {
            for (var i = 0; i < obj.returns.length; i++) {
                code.appendMarkdown("): " + obj.returns[0].type.names[0]);
            }
        }
        else {
            code.appendMarkdown("): void");
        }
    }
    if (obj.kind === "member") {
        code.appendMarkdown(": " + obj.meta.code.type)
    }
    return code.value;
}

function getDocumentation(obj) {
    if (obj.undocumented)
        return undefined
    const documentation = new vscode.MarkdownString();
    documentation.appendMarkdown(obj.description + "\n\n");
    if (obj.params != undefined) {
        for (var i = 0; i < obj.params.length; i++) {
            documentation.appendMarkdown("@param " + "`" + obj.params[i].name + "`");
            if (obj.params[i].description != undefined) {
                documentation.appendMarkdown(" " + obj.params[i].description);
            }
            documentation.appendMarkdown("\n\n");
        }
    }
    if (obj.properties != undefined) {
        for (var i = 0; i < obj.properties.length; i++) {
            documentation.appendMarkdown("@property " + "`" + obj.properties[i].name + "`");
            if (obj.properties[i].description != undefined) {
                documentation.appendMarkdown(" " + obj.properties[i].description);
            }
            documentation.appendMarkdown("\n\n");
        }
    }
    if (obj.returns != undefined) {
        for (var i = 0; i < obj.returns.length; i++) {
            if (obj.returns[0].description != undefined)
                documentation.appendMarkdown("@return " + obj.returns[0].description + "\n\n");
            else
                documentation.appendMarkdown("@return \n\n");
        }
    }
    /*
    if (obj.examples != undefined) {
        for (var i = 0; i < obj.examples.length; i++) {
                documentation.appendMarkdown("@examples " + beautify(obj.examples[i]) + "\n\n");
        }
    }
    */
    return documentation.value;
}

function GetKind(str) {
    switch (String(str)) {
        case "method":
            return vscode.CompletionItemKind.Method
        case "function":
            return vscode.CompletionItemKind.Function
        case "class":
            return vscode.CompletionItemKind.Class
        case "property":
            return vscode.CompletionItemKind.Property
        case "member":
            return vscode.CompletionItemKind.Variable
        case "event":
            return vscode.CompletionItemKind.Event
        default:
            return vscode.CompletionItemKind.Keyword
    }
}

function getCompletion(obj) {
    let Completion;
    if (obj.scope === "static") {
        Completion = new vscode.CompletionItem(obj.longname);
    } else {
        Completion = new vscode.CompletionItem(obj.name);
    }
    Completion.kind = GetKind(obj.kind)

    if (!obj.undocumented) {
        if (Completion.kind === vscode.CompletionItemKind.Function) {
            Completion.documentation = new vscode.MarkdownString("``` squirrel\n" + obj.head + "\n```\n" + obj.documentation);
        }
        if (Completion.kind === vscode.CompletionItemKind.Event) {
            Completion.documentation = new vscode.MarkdownString("``` squirrel\n" + obj.head + "\n```\n" + obj.documentation);
        }
    }
    return Completion;
}
