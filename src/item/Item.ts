import { CompletionItem, CompletionItemKind, TextDocument } from "vscode";

interface VariableItem {
    body: string,
    local: string,
    name: string,
    value: string
}

export default class Variable {
    list: VariableItem[] = [];

    constructor( document: TextDocument ) {
        let result;
        let regex = /(local|static)? ?([a-zA-Z_0-9]+)\s*=\s*([a-zA-Z_{1}][a-zA-Z0-9_]+)(?=\()/gm;
        while ( result = regex.exec( document.getText() ) ) {
            this.list.push( {
                body: result[0],
                local: result[1],
                name: result[2],
                value: result[3]
            } );
        }
    }

    getVariableCompletionArray(): CompletionItem[] {
        return this.list.map( item => new CompletionItem(
            { label: item.name, description: item.value },
            CompletionItemKind.Variable
        ) );
    }
}
