import * as vscode from 'vscode';
import { js_beautify } from 'js-beautify';
import {ProvideCompletionItems, ProvideCompletionItemsDOT} from './provider/provideCompletionItems';
import { ProvideCurrentCompletion } from './provider/provideCurrentCompletion';
import { ProvideHover } from './provider/provideHover';
import { ProvideOnTypeFormattingEdits } from './provider/provideOnTypeFormatting';

export function activate( context: vscode.ExtensionContext ) {
    const configuration = vscode.workspace.getConfiguration('vscode-squirrel', undefined)
    if ( configuration.get('OnTypeFormatting') ) {
        context.subscriptions.push(
            vscode.languages.registerOnTypeFormattingEditProvider(
                'squirrel',
                new ProvideOnTypeFormattingEdits(),
                '.',
                '=',
                ',',
                ';'
            )
        );
    }
    if ( configuration.get('Hover') ) {
        context.subscriptions.push(
            vscode.languages.registerHoverProvider(
                'squirrel',
                new ProvideHover()
            )
        );
    }
    if ( configuration.get( 'Completion' ) ) {
        context.subscriptions.push(
            vscode.languages.registerCompletionItemProvider(
                'squirrel',
                new ProvideCurrentCompletion()
            )
        );
        context.subscriptions.push(
            vscode.languages.registerCompletionItemProvider(
                'squirrel',
                new ProvideCompletionItems()
            )
        );
        context.subscriptions.push(
            vscode.languages.registerCompletionItemProvider(
                'squirrel',
                new ProvideCompletionItemsDOT(),
                '.'
            )
        );
    }

    context.subscriptions.push(vscode.languages.registerDocumentFormattingEditProvider('squirrel', {
        provideDocumentFormattingEdits(document, options, token) {
            const fullText = document.getText()
            const fullRange = new vscode.Range(document.positionAt(0), document.positionAt(fullText.length));
            return [vscode.TextEdit.replace(fullRange, js_beautify(fullText).replace(/local\s+function/g, "local function"))];
        }
    }));
    if ( configuration.get('TrailingWhitespace') ) {
        vscode.workspace.onWillSaveTextDocument( event => {
            let e = vscode.window.activeTextEditor;
            if (! e ) {
                return null;
            }

            let reg = /[ \t]+$/gm;
            const fullText = event.document.getText();
            const fullRange = new vscode.Range( event.document.positionAt(0), event.document.positionAt(fullText.length) );
            e.edit( builder => {
                builder.replace( fullRange, fullText.replace( reg, '' ) );
            });
        });
    }
}

export function deactivate() { }
