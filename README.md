# Squirrel Language Supports #
Syntax highlighing, completions and formatting support for squirrel language


# Features
* Syntax highlighting and bracket matching.
![highlighting](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/5f36f90f19ba0b6039a9edab240dc7da26416e2e/images/highlighting.gif)

* Simple completions.
![completion](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/5f36f90f19ba0b6039a9edab240dc7da26416e2e/images/completion.gif)

* Formatting.
![formatting](https://bitbucket.org/marcinbar91/vscode-squirrel/raw/5f36f90f19ba0b6039a9edab240dc7da26416e2e/images/formatting.gif)

* OnTypeFormatting. (must be checked editor.formatOnType in settings)

# License
You are free to use this in any way you want, in case you find this useful or working for you but you must keep the copyright notice and license. (MIT)


# Credits
* JS Beautifier <https://github.com/beautify-web/js-beautify/>
* Highlighting based on Microsoft JavaScript <https://github.com/microsoft/vscode/blob/master/extensions/javascript/syntaxes/JavaScript.tmLanguage.json>